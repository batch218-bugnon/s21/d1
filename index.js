// console.log("Hello World");

console.log("Variable vs Array");

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);
						// 0		//1           //2
let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1923"];
// inside the square bracket are what we called elements 
console.log(studentNumbers);
console.log("---------------------------");

// [SECTION] Arrays

/*
    - Arrays are used to store multiple related values in a single variable.
       - They are declared using square brackets ([]) also known as "Array Literals"
       - Arrays it also provides access to a number of functions/methods that help in manipulation array.
           - Methods are used to manipulate information stored within the same object.
       - Array are also objects which is another data type.
       - The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).
       - Syntax:
           let/const arrayName = [elementA, elementB, elementC, ... , ElementNth];
*/

console.log("Examples of Array: ");

	// Common examples of arrays
	let grades = [98.5, 94.3, 89.2, 90]; // arrays could store numberic values
	let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fugitsu"]; // Arrays could store string values 

	console.log(grades);
	console.log(computerBrands);

	// possible use of an array but it is not recommended
	// It is not recommended for it is hard to define the purpose of the array or what are data inside the array and where the programmer could use it. 
	let mixedArr = ["John", "Doe", 12, false, null, undefined, {}];
	console.log(mixedArr);


	// Alternative way of creating an array
	let myTasks = [
		"drink HTML",
		"eat javascript",
		"inhale css",
		"bake mongoDB"
	]

	console.log(myTasks);

	console.log("---------------------------");

	// Creating an array with values from variables
	let city1 = "tokyo";
	let city2 = "Manila";
	let city3 = "Jakarta";

	let cities = [city1, city2, city3];
	console.log(cities);
	console.log("---------------------------");

	// [SECTION] .length property
	// ".length" property allows us to get and set the total number of items in an array
	console.log("Using .length property for array size");
	console.log("Length / size of myTasks array: " + myTasks.length); // 4
	console.log("Length / size of cities array: " + cities.length);

	console.log("---------------------------");

	console.log("Using .length property for string size");

	let fullname = "Jerisha Iris B. Bugnon";
	console.log("Length/size of fullname variable: " + fullname.length); // spaces included 

	console.log("---------------------------");


	/*
	let myTasks = [
		"drink HTML",
		"eat javascript",
		"inhale css",
		"bake mongoDB"
	]

	*/
	console.log("Removing the last element from an array");

	myTasks.length = myTasks.length - 1;
	// 4              // 4
	console.log(myTasks.length);
	console.log(myTasks);


	// To delete a specific item in an array we can employ array methods. We have shown the logic or algorith of "pop method";


	// let cities = [city1, city2, city3]; // length 3
	cities.length--;
	console.log(cities);


	// We can't do the same on string
	// let fullname = "Jerisha Iris B. Bugnon";
	fullname.length = fullname.length - 1;
	console.log(fullname.length);
	console.log(fullname);


	console.log("---------------------------");


	// We can also add the length of an array.
	console.log("add an element to an array");
					// 0       // 1   //2      //3
	let theBeatles = ["John", "Paul", "Ringo", "George"];
	console.log(theBeatles);

	theBeatles[theBeatles.length] = "Cardo";
	console.log(theBeatles);



	console.log("---------------------------");


	// [SECTION] Reading from Arrays
   /*
       - Accessing array elements is one of the common task that we do with an array.
       - This can be done through the use of array indexes.
       - Each element in an array is associated with it's own index number.
       - The first element in an array is associated with the number 0, and increasing this number by 1 for every element.
       - Array indexes it is actually refer to a memory address/location

       Array Address: 0x7ffe942bad0
       Array[0] = 0x7ffe942bad0
       Array[1] = 0x7ffe942bad4
       Array[2] = 0x7ffe942bad8

       - Syntax
           arrayName[index];
   */


   // let grades = [98.5, 94.3, 89.2, 90];
   console.log(grades[0]); // The index number of the first element is zero

   // let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fugitsu"]; 
   console.log(computerBrands[3]); // counting in Js starts in zero


   // Accessing an array element that does not exist will return undefined
   console.log(grades[20]);	// undefined

   let lakerlegends=["Kobe", "Shaq", "Lebron", "Magic", "Kareem", "Westbrook"];

   console.log(lakerlegends[1]);
   console.log(lakerlegends[3]);


   console.log("---------------------------");


   console.log("Reassignin an element from an array");
   	// You can also reassign array values using indeces 

   	console.log("Array before Reassignment: ");
   	console.log(lakerlegends);

   	lakerlegends[2] = "Gasol";
   	console.log("Array after Reassignment: ");
   	console.log(lakerlegends);



   	console.log("---------------------------");

   	console.log("Access the last element of an array");

   	let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

   	let lastElementIndex = bullsLegends.length-1;
   	console.log(bullsLegends[lastElementIndex]);

   	console.log("---------------------------");

   	console.log("Addong a new item to an array using indeces");
   		// Adding items into an array
   		// We can add items in array using indeces 

   	const newArr = [];
   	console.log(newArr[0]); //undefined

   	newArr[0] = "Cloud strife";
   	console.log(newArr[0]);

   	console.log(newArr[1]); // undefined
   	newArr[1] = "Tifa Lockhart";
   	console.log(newArr[1]);

   	console.log("---------------------------");

   	console.log("Add element in the end of an array using .length property ");

   	//["Cloud strife", "Tifa Lockhart"]
   	newArr[newArr.length] = "Barret Wallace";
   	console.log(newArr);

   	console.log("---------------------------");
   	console.log("Displaying the content of an arrray 1 by 1");

   	// ["Cloud strife", "Tifa Lockhart", "Barret Wallace"];
   	
   	// Example of accessing array elements 1 by 1 with console.log only
   	// console.log(newArr[0]);
   	// console.log(newArr[1]);
	// console.log(newArr[2]);


	// ["Cloud strife", "Tifa Lockhart", "Barret Wallace"];
		//declaration  //condition   //
	for(let index = 0; index < newArr.length; index++){
		console.log(newArr[index]);
	}

   	
   	
	console.log("---------------------------");


	console.log("Filtering an array using loop and conditional statements: ");
	let numArr2 = [5, 12, 30, 46, 40, 52];

	// This code is to check which number is divisible by 5
	for (let index = 0; index < numArr2.length; index++){
		if(numArr2[index] % 5 == 0){
			console.log(numArr2[index] + " is divisible by 5.");
		}
		else {
			console.log(numArr2[index] + " is not divisible by 5.")
		}
	}


	// [SECTION] Multidimentional Arrays

/*
   -Multidimensional arrays are useful for storing complex data structures.
   - A practical application of this is to help visualize/create real world objects.
   - This is frequently used to store data for mathematic computations, image processing, and record management.
   - Array within an Array
*/


// Create chessboard
let chessBoard = [
   ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
   ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
   ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
   ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
   ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
   ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
   ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
   ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

// let chessboard = [array, array, array, array, array, array, array, array];

console.table(chessBoard);


// Access an element of a multidemensional arrays
// Syntax: multiArr[outerArr][innerArray]
					//col     //row

console.log(chessBoard[3][4]);

console.log("Pawn moves to: " + chessBoard[2][5]);






























	



